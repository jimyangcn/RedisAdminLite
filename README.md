# RedisAdminLite
**更新：**<br />
=== V2.0(2017-3-31)===<br />
重构为RedisAdminLite.<br />
=== V1.3(2015-10-8)===<br />
新增键名列表对2.8.0以下版本的兼容,优化查询结果的显示效果.<br />
<br />
**前言：**<br />
项目开发用到了Redis，但是在调试Redis数据的时候，没有一款通用的可视化管理工具。在网络找了一些，但是感觉功能上都不尽人意，于是决定抽出一点时间，开发一个用起来顺手的Redis管理工具。<br />
秉承自己开发的小工具一贯的风格，首先是要尽可能绿色单文件，尽量不依赖其他文件，然后是容易部署。 这样才能方便在项目中提高生产力!<br />
<br />
**简介：**<br />
RedisAdminLite前身是SuRedisAdmin_PHP，是一个用于在线管理Redis内存数据库的单文件绿色版PHP程序，和phpinfo.php一样可以方便放到项目中使用。
开发项目过程中，调试Redis是一个繁琐的体力劳动，如果有一个通用管理程序，可以很简单而高效地观察数据，那对项目的开发效率肯定有明显的提升，可以减少很多重复而单调的体力劳动。所以，在网络上找了几款Redis管理软件，但是为了简单查看一下Redis数据，却要安装一大堆不需要的依赖文件，而且更重要的是功能又用起来很不顺心，真是受够了。求人不如求己，决定自己抽出一点时间来开发一个自己用起来顺手的小工具，简单而高效。功能尽可能覆盖Redis提供的管理接口，因为Redis本身接口不是很多，但是界面一定要简洁易用，至少自己用起来得心应手。<br />
<br />
**特性：**<br />
1、单文件绿色版，无外部依赖，方便部署使用<br />
2、支持多个数据库切换<br />
3、支持用户登录验证、IP验证等多种权限验证功能<br />
4、支持内置配置文件，使用更灵活<br />
5、支持Redis大部分操作<br />
<br />
**使用：**<br />
1. 将文件复制到您的项目中任意目录(本文件为单文件绿色版,方便使用.<br />
2. 修改配置信息为适合您需要的内容.<br />
3. 运行本文件, 开始Redis调试<br />
<br />
项目主页：http://git.oschina.net/sochishun/RedisAdminLite <br />
交流博客：http://www.cnblogs.com/sochishun/p/4730742.html <br />
<br />

**快照：**<br />
<img alt="login.png" src="Snapshot/login.png" /><br /><br />
<img alt="main.png" src="Snapshot/main.png" /><br /><br />
<img alt="info.png" src="Snapshot/info.png" /><br /><br />
<img alt="search.png" src="Snapshot/search.png" /><br /><br />
<img alt="add.png" src="Snapshot/add.png" /><br /><br />
<img alt="config.png" src="Snapshot/config.png" /><br /><br />
